import * as execa from "execa";
import { outputChannel } from "./buck_logger";
import { getBuckPipeStdErr, getDefaultBuckExecutable, getConfig } from "./buck_config";
import * as stream from "stream";
import * as vscode from "vscode";

interface BuckOptions {
    cwd: string;                // The directory from which buck will run.
    cmd: string;                // Any of build, run, query, audit, etc.
    target?: string;
    flavor?: string;            // I.e 'linux-x86_64'.
    preTargetArgs?: string[];   // I.e "buck query [preTargetArgs] //:target"
    postTargetArgs?: string[];  // I.e "buck build //:target [postTargetArgs]"
    hideOutput?: boolean;
    ignoreExceptions?: boolean; // Error messages will not be presented to user if this value is true.
}

export async function Buck(options: BuckOptions) {
    try {
        let buckArgs: string[] = [options.cmd];

        if (options.preTargetArgs) {
            buckArgs = buckArgs.concat(options.preTargetArgs);
        }

        let flavor = "";

        if (options.flavor) {
            flavor = options.flavor.indexOf("#") === 0 ? options.flavor : "#" + options.flavor;
        }

        if (options.target) {
            buckArgs.push(options.target + flavor);
        }

        if (options.postTargetArgs) {
            buckArgs = buckArgs.concat(options.postTargetArgs);
        }

        const writableStdOutStream = new stream.Writable();
        writableStdOutStream._write = (chunk: any, _, next) => {
            outputChannel.append(`${chunk}`);
            next();
        };

        const buckExecutable = getDefaultBuckExecutable();
        if (! options.hideOutput) {
            outputChannel.appendLine("[ Start of buck invokation ]");
            outputChannel.appendLine(`{ cmd: \"${buckExecutable} ${buckArgs.join(" ")}\" | cwd: \"${options.cwd}\" }`);
        }

        const child = execa(buckExecutable, buckArgs, {
            cwd: options.cwd,
            shell: true,
            env: getConfig().env as NodeJS.ProcessEnv
        });

        if (! options.hideOutput) {
            child.stdout.pipe(writableStdOutStream);

            if (getBuckPipeStdErr()) {
                child.stderr.pipe(writableStdOutStream);
            }
        }

        const { stdout } = await child;

        if (! options.hideOutput) {
            outputChannel.appendLine("[ End of buck invokation ]\n");
        }

        return stdout.split("\n");
    } catch (err) {
        console.log(err);
        if (err.exitCode && !options.ignoreExceptions) {
            vscode.window.showErrorMessage(`buck exited with exit code ${err.exitCode}: ${err.message}\ncwd:${options.cwd}\nall:${err.all}`);
        }
    }

    return [];
}
