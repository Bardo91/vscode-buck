import * as vscode from "vscode";
import { Buck } from "./buck";
import * as buckConfig from "./buck_config";
import { outputChannel } from "./buck_logger";
import { CompilationDatabase, ICompilationDatabaseEntry } from "./buck_compilation_database";

interface IBuckInvocation {
    cwd: string;
    cmd: string;
    target: string;
    flavor: string;
    preTargetArgs: string[];
    postTargetArgs: string[];
}

class BuckTarget {
    constructor(public readonly project: BuckProject, public readonly name: string, private cwd: string) { }

    createBuckInvocationConfig(cwd: string, target: string): IBuckInvocation {
        return {
            cwd: cwd,
            cmd: "build",
            target: target,
            flavor: buckConfig.getPlatformFlavor(),
            preTargetArgs: buckConfig.getPreTargetArgs().concat(buckConfig.getBuckConfigurationArgs()),
            postTargetArgs: ([] as string[]).concat(buckConfig.getPostTargetArgs()),
        } as IBuckInvocation;
    }

    async build() {
        vscode.window.showInformationMessage(`Building ${this.name}..`);
        const options = this.createBuckInvocationConfig(this.cwd, this.name);

        options.postTargetArgs.push("--show-output");

        await Buck(options)
            .then((val: string[]) => vscode.window.showInformationMessage(val.join("\n")));

    }

    async run() {
        vscode.window.showInformationMessage(`Running ${this.name}..`);
        const options = this.createBuckInvocationConfig(this.cwd, this.name);
        options.cmd = "run";

        await Buck(options).then((val: string[]) => {
            vscode.window.showInformationMessage(`Done running ${this.name}`);
        });
    }

    async generateCompilationDatabase(progress: vscode.Progress<{ message?: string | undefined; increment?: number | undefined; }>, token: vscode.CancellationToken) {
        const cdbConfig = buckConfig.getCompilationDatabaseSettings();

        let shouldCancel: boolean = false;
        token.onCancellationRequested(() => {
            shouldCancel = true;
        });

        // Query target dependencies
        let dependencies: string[] = await Buck({
            cwd: this.project.currentProjectRoot(),
            cmd: "query",
            preTargetArgs: [],
            target: `\"deps('${this.name}', ${cdbConfig.depth > 0 ? cdbConfig.depth : 0}, first_order_deps())\"`,
            postTargetArgs: ["--output-format", "json"],
        });
        progress.report({
            increment: 10
        });
        outputChannel.show();

        if (dependencies.length > 0) {
            dependencies = JSON.parse(dependencies[0]) as string[];

            let duty: number[] = [70, 10, 10];
            const step = duty[0] / dependencies.length;

            //dependencies = dependencies.filter(x => x !== this.name);
            const options = this.createBuckInvocationConfig(this.cwd, this.name);
            options.flavor += ",compilation-database";
            options.postTargetArgs.push("--show-output");

            const compilationDatabases: CompilationDatabase[] = [];

            outputChannel.appendLine(`Generating a compilation-database for ${this.name} with a dependency depth of ${cdbConfig.depth}...`);
            // Build and merge compilation-databases
            for (const t of dependencies) {
                // Generate for current target
                try {
                    if (shouldCancel) {
                        break;
                    }

                    const compileCommands = await Buck({
                        ...options,
                        target: t,
                        hideOutput: true,
                        ignoreExceptions: true
                    });

                    const output: string[] = compileCommands[0].split(' ');
                    //outputChannel.appendLine(`Output for ${t}`);

                    if (output.length > 1) {
                        outputChannel.appendLine(`- ${output[1]}`);
                        compilationDatabases.push(new CompilationDatabase(this.cwd + '/' + output[1]));
                    }
                } catch (error) {
                    outputChannel.appendLine(`Got error for target ${t}, this target will be ignored.`);
                    continue;
                }

                progress.report({
                    increment: step
                });
            }

            // Flatten include directives
            if (cdbConfig.flatten) {
                progress.report({ message: "Flattening include directives.." });
                outputChannel.appendLine("Flattening include directives...");
                await Promise.all(compilationDatabases.map(x => x.flattenIncludeDirectives()));
                outputChannel.appendLine("Done flattening include directives...");
                progress.report({
                    increment: duty[1]
                });
            }

            if (shouldCancel) {
                return;
            }

            // Create a master compile-commands.json
            progress.report({ message: "Creating master compile_commands.json.." });
            const destFile = cdbConfig.outputFolder + '/compile_commands.json';
            outputChannel.appendLine(`Saving compilation-database at ${destFile}`);
            let masterDb = new CompilationDatabase(destFile);

            for (const cdb of compilationDatabases) {
                masterDb.db = masterDb.db.concat(cdb.db);
            }

            masterDb.saveFile();
            progress.report({ message: "compile_commands.json created", increment: duty[2] });

            if (cdbConfig.updateClangd) {
                outputChannel.appendLine(`Setting clangd's -compile-commands-dir=${cdbConfig.outputFolder}`);
                buckConfig.updateClangd();
            }

            if (cdbConfig.openGeneratedFile) {
                vscode.workspace.openTextDocument(masterDb.fsPath)
                    .then((document: vscode.TextDocument) => {
                        vscode.window.showTextDocument(document)
                            .then((x: vscode.TextEditor) => {
                                // TODO: Format compile-commands.json
                                // vscode.commands.executeCommand('vscode.executeFormatDocumentProvider', {uri: x.document.fileName})
                                // .then(() => {
                                //     x.document.save();
                                // });
                            });
                    });
            }
        }
    }
}

export class BuckProject {
    public targets: BuckTarget[] = [];
    private activeTarget: BuckTarget | null = null;
    private projectRoot: string = "";
    private sbItemBuckProject: vscode.StatusBarItem;
    private sbItemBuckTarget: vscode.StatusBarItem;
    private sbItemBuckBuild: vscode.StatusBarItem;

    constructor() {
        this.sbItemBuckProject = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, -99);
        this.sbItemBuckProject.color = vscode.ThemeColor;
        // this.sbItemBuckProject.show();

        this.sbItemBuckTarget = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, -100);
        this.sbItemBuckTarget.color = vscode.ThemeColor;
        this.sbItemBuckTarget.show();

        this.sbItemBuckBuild = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, -101);
        this.sbItemBuckBuild.color = vscode.ThemeColor;
        this.sbItemBuckBuild.command = "buck.buildTarget";
        this.sbItemBuckBuild.show();
    }

    currentProjectRoot(): string {
        return this.projectRoot;
    }

    getTarget(targetName: string): BuckTarget | null {
        const i = this.targets.findIndex(
            (value, i) => value.name === targetName
        );

        if (i >= 0) {
            return this.targets[i];
        }

        return null;
    }

    getCurrentTarget(): BuckTarget | null {
        return this.activeTarget;
    }

    setCurrentTarget(newTarget: BuckTarget | string | null) {
        if (typeof newTarget === "string") {
            // TODO: Improve target name comparison
            const target = this.targets.find((x, i, obj) => {
                if (x.name === (newTarget as string)) {
                    return x;
                }
                return false;
            });

            if (!target) {
                this.activeTarget = null;
                return false;
            }

            this.activeTarget = target;

        } else if (newTarget && this.targets.includes(newTarget)) {
            this.activeTarget = newTarget;
        } else {
            this.activeTarget = null;
        }

        if (this.activeTarget) {
            this.sbItemBuckTarget.text = `Buck: ${this.activeTarget.name}`;
            this.updateState();
            return true;
        }

        return false;
    }

    async init(newProjectRoot?: vscode.WorkspaceFolder | string) {
        this.sbItemBuckBuild.text = "$(beaker) Buck: build";
        this.sbItemBuckTarget.text = "Buck: initializing";
        this.sbItemBuckProject.text = `Buck: ${this.projectRoot}`;

        const tmpRoot = this.projectRoot;

        if (newProjectRoot && typeof newProjectRoot === "string") {
            this.projectRoot = newProjectRoot.split("/.buckconfig")[0];
        } else if (newProjectRoot) {
            this.projectRoot = (newProjectRoot as vscode.WorkspaceFolder).uri.fsPath;
        }

        const targetQuery = await Buck({
            cwd: this.projectRoot,
            cmd: "query",
            target: "//...",
            hideOutput: this.projectRoot.length === 0 ? false : true
        });

        this.targets = targetQuery.map(x => new BuckTarget(this, x.trim(), this.projectRoot));

        if (this.targets.length > 0 && (!this.activeTarget || this.projectRoot !== tmpRoot)) {
            this.activeTarget = this.targets[0];
        }

        const state = buckConfig.getWorkspaceState();
        if (state && buckConfig.shouldUseWorkspaceState()) {
            console.log(`State found for ${this.projectRoot}`);
            const idx = this.targets.findIndex((value, index) => {
                return value.name === state.target;
            });

            if (idx >= 0) {
                this.activeTarget = this.targets[idx];
            }
        }

        if (this.projectRoot !== tmpRoot && buckConfig.shouldUseWorkspaceState()) {
            this.updateState();
        }

        this.sbItemBuckTarget.command = "buck.selectTarget";

        if (this.activeTarget) {
            this.sbItemBuckTarget.text = `Buck: ${this.activeTarget.name}`;
            this.sbItemBuckTarget.tooltip = this.projectRoot + "/.buckconfig";
        }
    }

    updateState() {
        if (buckConfig.shouldUseWorkspaceState()) {
            buckConfig.storeWorkspaceState({
                projectPath: this.projectRoot,
                flavor: [],
                target: this.activeTarget ? this.activeTarget.name : "",
                platform: "linux-x86_64"
            });
        }
    }
}
