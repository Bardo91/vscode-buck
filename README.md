# Buck Build extension for Visual Studio Code

This extension provides basic [Buck Build](https://buck.build) support for vscode, inspired by and loosely based on the vscode-bazel extension.

    Warning: this extension is a work in progress and its features can change when being updated.
    This is a personal project and the author has no association with Facebook or the Buck project.

## Features

- [x] Select project
- [x] Select target
- [x] Buck build
- [x] Buck run
- [x] Buck clean
- [x] Buck query
- [x] Buck audit
- [x] Snippets for `BUCK` files - *cxx_binary*, *cxx_library*

*Work-in-progress, features are partially implemented*.

## Requirements

In order to use this extension you will need to install [Buck Build](https://buck.build). The path to `buck` executable should be available in your system PATH environmental variable or explicitly set in the `buck.buckExecutable` configuration setting.

## Documentation

##### Configuration

| Option                        |                                                                                                                                                                                                                            |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `buck.executable`             | Path of buck executable path. Default: Looks for `buck` on `PATH.`                                                                                                                                                         |
| `buck.platform`               | The platform flavor to build. If empty the host platform will be built.                                                                                                                                                    |
| `buck.env`                    | Environmental variables (key-value pairs) to use when invoking the buck executable.                                                                                                                                        |
| `buck.preTargetArgs`          | An array of extra pre-target arguments that will be inserted on the final buck command. `buck build {preTargetArgs} //:target`.                                                                                            |
| `buck.configArgs`             | An array of (optional) buck config arguments. Each entry will be prepended by `-c` such that `["*//cxx.cxx=/usr/bin/clang++"]`will result in the buck build command `buck build -c *//cxx.cxx=/usr/bin/clang++ //:target`. |
| `buck.postTargetArgs`         | An array of extra post-target arguments that will be inserted on the final buck command. `buck build //:target {postTargetArgs}`.                                                                                          |
| `buck.pipeStderr`             | Pipe stderr to vscode-buck output channel.                                                                                                                                                                                 |
| `buck.autoFocusOutputChannel` | Show/focus output channel on each buck invocation.                                                                                                                                                                         |
| `buck.projectConfig`          | The config of the last selected buck project. This will change automatically when you select a new project or update target and/or flavor.                                                                                 |
|                               |                                                                                                                                                                                                                            |

### Commands

##### Select Project

Provides a drop-down list of projects  (directories containing a `.buckconfig`file) and sets the selected project as the active Buck project.

##### Select Target

Sets the active target with respect to the currently selected Buck project.

##### Build

Builds the active target and writes `stdout` output to `Output -> Buck`.

On failures a warning message will be shown in VS Code.

##### Build And Run Selected Target

##### Query

##### Audit

### Compilation Database

Buck can generate compilation-databases for your C++ targets by appending `#compilation-database` to the target in your `buck build` command. This extension adds some useful options to the generation of your `compile_commands.json` to improve compatibility with third-party tools such as `clangd`, and allow you to generate compilation-databases with transitive dependencies included in the final `compile_commands.json` file.

###### `buck.compilationDatabase.outputFolder`

- This setting specifies where to put the generated `compile_commands.json` file. `absolute/path/to/json/folder`. Default location is the workspace root.

###### `buck.compileCommands.flatten`

- Flattens include directives that is added to the compilation-database using the `@/path/to/genrule/output.txt` syntax. This option will attempt to replace include directive entries such as `"@/home/john/freetype2.txt"` with its file content i.e `"-lfreetype"`.

###### `buck.compileCommands.dependencyDepth`

- Setting a depth higher than 0 will merge in the target's transitive dependencies in the generated compilation-database. This is useful when working on larger projects and you want *'recursive'* intellisense including your target's dependencies.

###### `buck.compileCommands.openGeneratedFile`

- When activated, this option will automatically open the generated `compile_commands.json` in your editor immediately after its creation.

###### `buck.compileCommands.updateClangdCompileCommandsDir`

- This option requires `vscode-clangd` to be installed, and when activated it will automatically set or update the `-compile-commands-dir=` in your workspace's `clangd.arguments` settings to reflect the newly generated `compile_commands.json` file.

- *This option will replace the original `-compile-commands-dir` entry of your clangd settings, but should preserve additional arguments.*

## Known Issues

- N/A

## Release Notes

### 0.1.5

- Added command `buck.refresh` to rebuild target list.

### 0.1.4

- Make immutable copy of postTargetArgs array to prevent modification of configuration content. Fixes faulty Buck Run command.
- Hide Buck commands from command palette when no project is selected and loaded.

### 0.1.3

- Added progress bar to generation of `compile_commands.json`.

### 0.1.2

- Updated README.md
- Removed unused configuration options.
- Added option `buck.useProjectConfig` (default: true)
- Handle cancelled commands (`buck.query`, `buck.audit`)
- Added buck-icon for `WIP` action bar view.

### 0.1.1

- Fixed error in `buck.projectConfig`.
- Added option to add output folder of generated `compile_commands.json` as argument to `vscode-clangd`.

### 0.1.0

- Fixed a couple of missing commas in snippets.
- Added additional configuration/options for generation of `compile-commands.json`.

### 0.0.9

- Code refactoring
- Added config `buck.processCompilationDatabase` to replace include directives such as `"@/home/john/freetype2.txt"` with its content i.e `"-lfreetype"`
